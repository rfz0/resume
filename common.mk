SOURCE = $(wildcard *.tex)
TARGET = $(patsubst %tex,%pdf,$(SOURCE))
LATEXMK = latexmk

.PHONY: all clean

all: $(TARGET)

%.pdf: %.tex
	$(LATEXMK) -pdf -pdflatex='pdflatex -interaction=nonstopmode' \
		-use-make $<

view: $(TARGET)
	xdg-open $<

preview: $(SOURCE)
	$(LATEXMK) -pvc -dvi $<

clean:
	$(LATEXMK) -C
