NAME = My.Name.Resume
PDFS = cover/cover.pdf resume/resume.pdf
TARGET = $(NAME).pdf

.PHONY: all clean

all: $(TARGET)

$(TARGET): $(PDFS)
	gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=$@ $^

%.pdf: %.tex
	make -C $(dir $<)

view: $(TARGET)
	xdg-open $<

clean:
	$(foreach i,$(PDFS),make -C $(dir $(i)) clean;)
	rm -f $(PDFS) $(TARGET)
