## LaTeX Resume

A simple resume

## Usage
Edit resume.tex, cover.tex, and mymacros.sty

Generate PDF

	make NAME=My.Name.Resume

Preview document and continuously update after each save (using xdvi)

	cd resume (or cover)
	make preview

Open PDF in preferred application

	make view

Remove generated files

	make clean

